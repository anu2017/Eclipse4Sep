/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filehandling;

import java.util.logging.Level;
import java.util.logging.Logger;


    
class Hi1 implements Runnable
{   
   
    @Override
    public void run()
    {
        for(int i=0;i<10;i++)
        {   try {
            Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Hi.class.getName()).log(Level.SEVERE, null, ex);
           }
            System.out.println(i + " Hi");
    
        }
    }    
    
}

class Hello1 implements Runnable
{
    @Override
    public void run()
    {
        for(int j=0;j<10;j++)
        {    try {
            Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Hello.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(j + " Hello");
    
        }
    }
    
}

public class RunnableThreadDemo {

   
    public static void main(String[] args) {
        Thread t1 = new Thread(new Hi1());
        Thread t2 = new Thread(new Hello1());
        
        t1.start();
        t2.start();
//        Hi h1 = new Hi();
//        Hello h2 = new Hello();
//        
//        h1.start();
//        h2.start();
        
        
        
        
    }
    
}

