/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filehandling;

class InvalidAgeException extends Exception
{
    InvalidAgeException(String msg)
    {
        System.out.println(msg);
    }
}
public class TestThrow1{  
   static void validate(int age) throws InvalidAgeException{  
     if(age<18)  
      //throw new ArithmeticException("not valid");  
         throw new InvalidAgeException("Array Limit Exceeded");
     else  
      System.out.println("welcome to vote");  
   }  
   public static void main(String args[]){  
      try{
          validate(2);  
      }
      catch(InvalidAgeException iae)
      {
          iae.printStackTrace();
      }
      System.out.println("rest of the code...");  
  }  
}  
