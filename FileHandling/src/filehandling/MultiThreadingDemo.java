
package filehandling;

import java.util.logging.Level;
import java.util.logging.Logger;

class Hi extends Thread
{
    @Override
    public void run()
    {
        for(int i=0;i<10;i++)
        {   try {
            Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Hi.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(i + " Hi");
    
        }
    }    
    
}

class Hello extends Thread
{
    public void run()
    {
        for(int j=0;j<10;j++)
        {    try {
            Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Hello.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(j + " Hello");
    
        }
    }
    
}

public class MultiThreadingDemo {

   
    public static void main(String[] args) {
        Hi h1 = new Hi();
        Hello h2 = new Hello();
        
        h1.start();
        h2.start();
        
        
    }
    
}
