/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filehandling;

import java.util.Scanner;

public class ExceptionDemo {
    public static void main(String[] args) {
        int x = 0;
        System.out.println("Please enter number to find square root");
        Scanner sc = new Scanner(System.in);
        try {
            x = Integer.parseInt(sc.nextLine());
           
        }
        catch (NumberFormatException nfx){
            System.out.println("Please enter number format only.");
        }
        catch(ArrayIndexOutOfBoundsException aeofb)
        {
            System.out.println("Array Index is out of Bound");
        }
        catch(Exception e)
        {
            System.out.println("There is some generic error");
        }
        finally{
            System.out.println("This runs always");
        }
        System.out.println("Sq Root :: " + x*x);
        //System.out.println(args[0]);
    }
}
