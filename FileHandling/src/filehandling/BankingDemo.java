/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filehandling;

import java.util.logging.Level;
import java.util.logging.Logger;

class Bank{

    int balance;
    Bank()
    {
        System.out.println("Accout opened with balance 1000");
        this.balance = 1000;
    }
    public synchronized void deposit(int amount) {
        this.balance = this.balance + amount;
        System.out.println("Amount Deposited " + amount + "\n" + "Current balance is " + this.balance);
        notify();
    }

    public synchronized void withdraw(int amount) throws InterruptedException {
        if(amount > this.balance){
            wait();
        }
        this.balance = this.balance - amount;
        System.out.println("Amount Withdrawn " + amount + "\n" + "Current balance is " + this.balance);
    }
    public void showBalance()
    {
        System.out.println("Current Balance :: " + this.balance);
    }
    
}

class DepositThread extends Thread
{
    Bank b;
    int amount;
    DepositThread(Bank b,int amount)
    {
        this.b = b;
        this.amount = amount;
    }
    public void run()
    {
        this.b.deposit(2000);
    }
}

class WithdrawThread extends Thread
{
    Bank b;
    int amount;
    WithdrawThread(Bank b,int amount)
    {
        this.b = b;
        this.amount = amount
                
                
                ;
    }
    public void run()
    {
        try {
            this.b.withdraw(this.amount);
        } catch (InterruptedException ex) {
            Logger.getLogger(WithdrawThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

public class BankingDemo {

    public static void main(String[] args) {
        Bank bd = new Bank();
        DepositThread dt = new DepositThread(bd,2000);
        WithdrawThread wt = new WithdrawThread(bd,2000);
        dt.start();
        wt.start();
//        bd.deposit(1000);
//   
//        bd.withdraw(1000);
        
        
        

    }
}
