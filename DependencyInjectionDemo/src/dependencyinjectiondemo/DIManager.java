/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dependencyinjectiondemo;

/**
 *
 * @author ICIT
 */
public class DIManager {
    Shape s;
    public DIManager(Shape sobj)
    {
        this.s = sobj;
    }
    public void callArea()
    {
        this.s.area();
    }
}
