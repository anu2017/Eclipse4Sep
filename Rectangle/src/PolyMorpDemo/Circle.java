/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PolyMorpDemo;

/**
 *
 * @author ICIT
 */
public class Circle extends Shape{
    int radius;
    
    Circle(int r)
    {
        this.radius = r;
    }
    @Override
    void area() {
        
        double area = 3.14 * (this.radius * this.radius);
        System.out.println("Area :: " + area);
        
    }
}
